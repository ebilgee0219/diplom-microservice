package com.mn.langdetect.model;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDTO {
    private String word;
    private String stopWordType;
}