package com.mn.langdetect.controller;

import com.mn.langdetect.model.ResponseDTO;
import com.mn.langdetect.service.ApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("service")
@RequiredArgsConstructor
public class LangDetectController {
    private final ApiService apiService;
    @PostMapping(value = "lang-detect")
    public ResponseEntity<List<ResponseDTO>> analyzeText(@RequestBody String context) {
        List<ResponseDTO> result =  this.apiService.callNLP(context);
        return ResponseEntity.ok(result);
    }
}
