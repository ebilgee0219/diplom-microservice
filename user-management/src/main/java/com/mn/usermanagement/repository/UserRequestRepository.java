package com.mn.usermanagement.repository;

import com.mn.usermanagement.model.UserRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRequestRepository extends JpaRepository<UserRequest,Long> {
    Page<UserRequest> findUserRequestsBy(Pageable pageable);
    List<UserRequest> findAllByUser_Id(Long id);
}
