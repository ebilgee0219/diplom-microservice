package com.mn.usermanagement.repository;

import com.mn.usermanagement.model.ApiToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface ApiTokenRepository extends JpaRepository<ApiToken, Long> {

    Optional<ApiToken> findFirstByToken(String token);

    Optional<ApiToken> findFirstByUser_Id(Long id);



}
