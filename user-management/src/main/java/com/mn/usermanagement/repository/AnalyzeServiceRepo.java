package com.mn.usermanagement.repository;

import com.mn.usermanagement.model.AnalyzeService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AnalyzeServiceRepo extends JpaRepository<AnalyzeService,Long> {
    Optional<AnalyzeService> findFirstById(Long id);
}
