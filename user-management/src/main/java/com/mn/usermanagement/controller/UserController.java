package com.mn.usermanagement.controller;

import com.mn.usermanagement.exception.ValidationError;
import com.mn.usermanagement.model.UserRequest;
import com.mn.usermanagement.model.dto.UserRequestDTO;
import com.mn.usermanagement.model.enumrations.Role;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.dto.UserDTO;
import com.mn.usermanagement.service.UserRequestService;
import com.mn.usermanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;
import java.util.List;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserRequestService userRequestService;
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private Environment env;

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @GetMapping("/service/port")
    public String getPort() {
        return "Service port number: " + env.getProperty("local.server.port");
    }


    @PostMapping("/service/registration")
    public ResponseEntity<UserDTO> saveUser(@RequestBody UserDTO dto) {
        User user = UserDTO.convertToEntity(dto, null);
        System.out.println(dto.toString());
        if (userService.findByUsername(user.getUsername()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        user.setRole(Role.USER);
        user = userService.save(user);
        return ResponseEntity.ok(UserDTO.convertToDTO(user));
    }

    @GetMapping("/service/login")
    public ResponseEntity<?> getUser(Principal principal) {
        if (principal == null || principal.getName() == null) {
            logger.info("null");
            //logout succesfull
            return new ResponseEntity<>(HttpStatus.OK);
        }
        User user = userService.findByUsername(principal.getName());

        return ResponseEntity.ok(UserDTO.convertToDTO(user));

    }

    @PostMapping("/service/names")
    public ResponseEntity<?> getNamesOfUsers(@RequestBody List<Long> idList) {
        return ResponseEntity.ok(userService.findUsers(idList));
    }

    @PostMapping("/service/id")
    public ResponseEntity<?> getUserId(@RequestBody String username) {
        User user = userService.findByUsername(username);
        return ResponseEntity.ok(user.getId());
    }

    @GetMapping("/service/test")
    public ResponseEntity<?> test() {
        return ResponseEntity.ok("It it working..");
    }

    @GetMapping("/service/usernames")
    public ResponseEntity<?> getAllUsername() {
        return ResponseEntity.ok(this.userService.findAllUsername());
    }

    @GetMapping("/service/user-history/{userId:[0-9]+}")
    public ResponseEntity<List<UserRequest>> getUserRequests(@PathVariable(value = "userId") Long userId) {
        List<UserRequest> data = this.userRequestService.getAllByUserId(userId);
        return ResponseEntity.ok(data);
    }
    @PostMapping("/service/user-request")
    public ResponseEntity<UserRequestDTO> createUserRequest(@RequestBody UserRequestDTO dto) {
        UserRequest userRequest = this.userRequestService.saveUserRequest(dto);
        return ResponseEntity.ok(UserRequestDTO.convertToDTO(userRequest,null));
    }
}
