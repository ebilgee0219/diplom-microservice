package com.mn.usermanagement.controller;

import com.mn.usermanagement.helper.Utils;
import com.mn.usermanagement.model.ApiToken;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.dto.ApiTokenDTO;
import com.mn.usermanagement.service.ApiTokenService;
import com.mn.usermanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@RestController
@RequiredArgsConstructor
public class ApiTokenController {
    private final ApiTokenService apiTokenService;
    private final UserService userService;
    private Logger logger = LoggerFactory.getLogger(ApiTokenController.class);

    @GetMapping("service/generate-token/{userId:[0-9]+}")
    public ResponseEntity<ApiToken> createApiToken(@PathVariable(value = "userId") Long userId) {
        User user = userService.findById(userId);
        String token = Utils.generateString();
        ApiToken apiToken = new ApiToken();
        apiToken.setUser(user);
        apiToken.setToken(token);
        apiToken.setStatus(true);
        return ResponseEntity.ok(apiTokenService.saveOrFail(apiToken, "Хадгалахад алдаа гарлаа"));
    }

    @PostMapping("service/token")
    public ResponseEntity<String> getApiKey(@RequestBody Long userId) {
        ApiToken apiToken = this.apiTokenService.findFirstByUser(userId);
        return ResponseEntity.ok(apiToken.getToken());
    }

    @PostMapping("service/api-token")
    public ResponseEntity<ApiTokenDTO> getApiTokenByToken(@RequestBody String token) {
        ApiToken apiToken = this.apiTokenService.findByTokenOrFail(token, "Олдсонгүй");

        return ResponseEntity.ok(ApiTokenDTO.convertToDTO(apiToken));
    }

    @PutMapping("service/refresh-token/{userId:[0-9]+}")
    public ResponseEntity<ApiToken> updateToken(@PathVariable(value = "userId") Long userId) {
        ApiToken apiToken = this.apiTokenService.findFirstByUser(userId);
        Date newEndDate = apiToken.getEndDate();
        newEndDate = Utils.addDate(newEndDate, 14);
        apiToken.setStartDate(apiToken.getEndDate());
        apiToken.setEndDate(newEndDate);
        return ResponseEntity.ok(this.apiTokenService.saveOrFail(apiToken, "Засварлахад алдаа гарлаа"));
    }
}
