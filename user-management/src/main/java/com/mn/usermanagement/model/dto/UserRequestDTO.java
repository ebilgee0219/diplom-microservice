package com.mn.usermanagement.model.dto;

import com.mn.usermanagement.model.UserRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRequestDTO {
    private Long userId;
    private List<String> usedServiceNames;
    private Date usedDate;
    private String context;

    public static UserRequest convertToEntity(UserRequestDTO dto) {
        UserRequest userRequest = new UserRequest();
        userRequest.setContext(dto.getContext());
        userRequest.setUsedDate(dto.getUsedDate());
        userRequest.setUserServiceName(String.join(",",dto.getUsedServiceNames()));
        return  userRequest;
    }
    public static  UserRequestDTO convertToDTO(UserRequest model, UserRequestDTO dto) {
        if(Objects.isNull(dto)) dto =new UserRequestDTO();
        dto.setContext(model.getContext());
        dto.setUsedDate(model.getUsedDate());
        dto.setUserId(model.getUser().getId());
        return dto;
    }
}
