package com.mn.usermanagement.model.dto;

import com.mn.usermanagement.model.User;
import lombok.Builder;
import lombok.Data;

import java.util.Objects;

@Data
@Builder
public class UserDTO {
    private Long id;
    private String username;
    private String email;
    private String apiToken;

    public static User convertToEntity(UserDTO dto, User user) {
        if(Objects.isNull(user))
            user= User.builder().build();
        user.setUsername(dto.getUsername());
        user.setEmail(dto.getEmail());
        return  user;

    }

    public static UserDTO convertToDTO(User user) {
        UserDTO userDTO= UserDTO.builder()
                .id(user.getId())
                .username(user.getUsername())
                .apiToken(user.getApiToken().getToken())
                .build();
        return userDTO;
    }
}
