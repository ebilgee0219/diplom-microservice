package com.mn.usermanagement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mn.usermanagement.helper.Utils;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Data
@Entity(name = "user_request")

public class UserRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @JsonIgnore
    @OneToMany( mappedBy = "userRequest")
    private List<AnalyzeService> analyzeServices;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date usedDate;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column
    private String context;

    @Column
    private String userServiceName;
    @PrePersist
    public void onPrepersist() {
        if(Objects.isNull(this.usedDate)) {
            Date now = new Date();
            this.usedDate = Utils.formatDate(now);
        } else {

            this.usedDate = Utils.formatDate(this.usedDate);
        }

    }
}
