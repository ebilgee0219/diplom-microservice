package com.mn.usermanagement.model.enumrations;

public enum Role {
    USER,
    ADMIN
}
