package com.mn.usermanagement.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mn.usermanagement.model.ApiToken;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ApiTokenDTO {
    private Long id;
    private Long userId;
    private String token;
    private Boolean status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;

    public static ApiTokenDTO convertToDTO(ApiToken model) {
        ApiTokenDTO apiTokenDTO = ApiTokenDTO.builder()
                .id(model.getId())
                .userId(model.getUser().getId())
                .startDate(model.getStartDate())
                .endDate(model.getEndDate())
                .token(model.getToken())
                .status(model.getStatus())
                .build();
        return  apiTokenDTO;
    }

}
