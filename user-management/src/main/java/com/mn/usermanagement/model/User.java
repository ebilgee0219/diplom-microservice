package com.mn.usermanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mn.usermanagement.model.enumrations.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Column(name = "email")
    private String email;

    @JsonIgnore
    @Enumerated(value = EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @JsonIgnore
    @OneToOne(mappedBy = "user", cascade = CascadeType.REMOVE)
    private ApiToken apiToken;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private List<UserRequest> userRequests;
}