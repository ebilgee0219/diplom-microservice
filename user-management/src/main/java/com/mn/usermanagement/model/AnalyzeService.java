package com.mn.usermanagement.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name="service")
public class AnalyzeService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="name")
    private String name;

    @ManyToOne
    private UserRequest userRequest;

}
