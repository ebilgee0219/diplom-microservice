package com.mn.usermanagement.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mn.usermanagement.helper.Utils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "api_token")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ApiToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "token", length = 50, unique = true, nullable = false)
    @NotBlank
    @Size(max = 50)
    private String token;

    @Column(name = "status", nullable = false)
    private Boolean status;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @JsonIgnore
    @OneToOne
    @MapsId
    private User user;

    @PrePersist
    public void onPrepersist() {
        Date startDate = new Date();
        this.startDate = Utils.formatDate(startDate);
        Calendar c = Calendar.getInstance();
        c.setTime(this.startDate);
        c.add(Calendar.DAY_OF_MONTH, 7);
        this.endDate = c.getTime();

    }

}
