package com.mn.usermanagement;

import com.mn.usermanagement.model.AnalyzeService;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.UserRequest;
import com.mn.usermanagement.repository.AnalyzeServiceRepo;
import com.mn.usermanagement.service.AnalysisService;
import com.mn.usermanagement.service.UserRequestService;
import com.mn.usermanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AppRunner implements CommandLineRunner {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    AnalysisService analysisService;
    @Autowired
    UserService userService;
    @Autowired
    UserRequestService userRequestService;
    @Override
    public void run(String... args) throws Exception {
        logger.info("--------> start <----------");

    }
}
