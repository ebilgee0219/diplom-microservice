package com.mn.usermanagement.service;

import com.mn.usermanagement.exception.ResourceNotFound;
import com.mn.usermanagement.exception.ValidationError;
import com.mn.usermanagement.model.ApiToken;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.repository.ApiTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class ApiTokenService {

    private final ApiTokenRepository apiTokenRepository;
    private final UserService userService;
    private Logger logger = LoggerFactory.getLogger(ApiTokenService.class);
    public ApiTokenService(ApiTokenRepository apiTokenRepository, UserService userService) {
        this.apiTokenRepository = apiTokenRepository;
        this.userService = userService;
    }

    public ApiToken saveOrFail(ApiToken apiToken, String msg) {
        try {
            return this.apiTokenRepository.save(apiToken);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new ValidationError(msg);
        }
    }

    public ApiToken findFirstByUser(Long id) {
        return this.apiTokenRepository.findFirstByUser_Id(id).orElseThrow(ResourceNotFound::new);
    }
    @Cacheable("token")
    public ApiToken findByTokenOrFail(String token, String message) {
        return this.apiTokenRepository.findFirstByToken(token).orElseThrow(()-> new ResourceNotFound(message));
    }



}
