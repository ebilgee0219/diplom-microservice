package com.mn.usermanagement.service;

import com.mn.usermanagement.exception.ValidationError;
import com.mn.usermanagement.helper.Utils;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.UserRequest;
import com.mn.usermanagement.model.dto.UserRequestDTO;
import com.mn.usermanagement.repository.UserRequestRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@RequiredArgsConstructor
@Getter
@Setter
public class UserRequestService {
    private final UserRequestRepository repository;
    private final UserService userService;
    private int pageSize=15;
    public UserRequest saveOrFail(UserRequest userRequest, String msg) {
        try {
         return    repository.save(userRequest);
        } catch (Exception e) {
            throw new ValidationError(msg);
        }
    }
    public Page<UserRequest> findUserRequestsBy(Integer page, Integer size, String sortBy) {

        return this.repository.findUserRequestsBy(getPageable(page,size,sortBy));
    }

    public List<UserRequest> getAllByUserId(Long userId) {
        try {
        return this.repository.findAllByUser_Id(userId);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Үр дүн олдсонгүй");
        }
    }
    public Pageable getPageable(Integer page, Integer size, String sortBy) {
        size = Utils.IsNullOrNegative(size) ? this.pageSize : size;
        page = Utils.IsNullOrNegative(page) ? 0 : page;
        Pageable paging = PageRequest.of(page,size, Sort.by(sortBy));
        return paging;
    }
    public UserRequest saveUserRequest(UserRequestDTO dto) {
        User user = this.userService.findById(dto.getUserId());
        UserRequest userRequest = UserRequestDTO.convertToEntity(dto);
        userRequest.setUser(user);
        return this.saveOrFail(userRequest,"Хадгалах үед алдаа гарлаа");
    }
}
