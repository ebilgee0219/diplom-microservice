package com.mn.usermanagement.service;

import com.mn.usermanagement.exception.ResourceNotFound;
import com.mn.usermanagement.exception.ValidationError;
import com.mn.usermanagement.helper.Utils;
import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.UserRequest;
import com.mn.usermanagement.repository.UserRepository;
import com.mn.usermanagement.repository.UserRequestRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
@Getter
@Setter
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRequestRepository userRequestRepository;
    private static int pagesize = 15;
    @Override
    public User save(User user) {
        if (Objects.nonNull(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            throw new ValidationError("PASSWORD");
        }
        return userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        if (Objects.isNull(username))
            throw new ValidationError("NULL USERNAME");

        return userRepository.findByUsername(username).orElseThrow(ResourceNotFound::new);
    }

    @Override
    public List<String> findUsers(List<Long> idList) {
        return userRepository.findByIdList(idList);
    }

    @Override
    public List<String> findAllUsername() {
        return this.userRepository.findAllUserName();
    }

    @Override
    public User findById(Long id) {
        return this.userRepository.findFirstById(id).orElseThrow(ResourceNotFound::new);
    }


}
