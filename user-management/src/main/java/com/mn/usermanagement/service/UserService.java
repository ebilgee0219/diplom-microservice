package com.mn.usermanagement.service;

import com.mn.usermanagement.model.User;
import com.mn.usermanagement.model.UserRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    User save(User user);

    User findByUsername(String username);

    List<String> findUsers(List<Long> idList);

    List<String> findAllUsername();

    User findById(Long id);
}
