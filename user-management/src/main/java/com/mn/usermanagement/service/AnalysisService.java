package com.mn.usermanagement.service;

import com.mn.usermanagement.exception.ValidationError;
import com.mn.usermanagement.model.AnalyzeService;
import com.mn.usermanagement.repository.AnalyzeServiceRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnalysisService {
    private final AnalyzeServiceRepo repo;

    public AnalyzeService findById(Long id) {
        return this.repo.findFirstById(id).orElseThrow(ValidationError::new);
    }

    public AnalyzeService saveOrFail(AnalyzeService analyzeService,String msg) {
        try {
            return this.repo.save(analyzeService);
        } catch (Exception e) {
            throw new ValidationError(msg);
        }
    }
}
