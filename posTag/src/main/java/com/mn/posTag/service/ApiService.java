package com.mn.posTag.service;

import com.mn.posTag.model.ResponseDTO;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApiService {

    private final RestTemplate restTemplate;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<ResponseDTO> callNLP(String text) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("text", text);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        String response = null;
        try {
            String url = "http://172.104.34.197/nlp-web-demo/process";
            response = restTemplate.postForObject(url, request, String.class);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        String posTag = "posTag";
        return filterList(response, posTag);
    }

    private List<ResponseDTO> filterList(String json, String attribute) {
        List<ResponseDTO> list = new ArrayList<>();
        try {
            JSONArray jsonArray  = new JSONArray(json);
            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                JSONArray jsonArray1 = jsonArray.getJSONArray(i);
                for (int j = 0; j < jsonArray1.length(); j++) {
                    JSONObject jsonObject = jsonArray1.getJSONObject(j);
                    list.add(new ResponseDTO(jsonObject.getString(attribute)));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


}
