package com.mn.nameType.contoller;

import com.mn.nameType.model.ResponseDTO;
import com.mn.nameType.service.ApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("service")
@RequiredArgsConstructor
public class ApiController {
    private final ApiService apiService;
    @PostMapping(value = "name-type-detect")
    public ResponseEntity<List<ResponseDTO>> analyzeText(@RequestBody String context) {
        List<ResponseDTO> result =  this.apiService.callNLP(context);
        return ResponseEntity.ok(result);
    }
}
