import axios from 'axios';
import {BehaviorSubject} from 'rxjs';
const API_URL = 'http://localhost:8765/api/user/service/'
const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')))
class UserService {
    get currentUserValue() {
        return currentUserSubject.value;
    }
    get currentUser() {
        return currentUserSubject.asObservable();
    }
   async login(user) {
        const headers = {
            authorization: 'Basic ' + btoa(user.username + ':' + user.password),
        };
        return axios.get(API_URL +'login', {headers: headers}).then(response => {
            localStorage.setItem('currentUser', JSON.stringify(response.data));
            currentUserSubject.next(response.data);
        });
    }
  async logout() {
        return axios.post(API_URL +'logout',{}).then( () => {
            localStorage.removeItem('currentUser');
            currentUserSubject.next(null);
        })
    }
   async register(user) {
        return axios.post(API_URL +'registration',user,
        {headers:{'Content-Type':'application/json; charset=UTF-8'}});
    }

    async userHistory(id) {
    return axios.get(`${API_URL}/user-history/${id}`);
    }

}
export default new UserService();