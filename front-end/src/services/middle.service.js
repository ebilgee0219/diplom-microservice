import axios from "axios";

const API_URL = "http://localhost:8765/api/middle/service/";
const API_NLP = "http://localhost:8002/service/lang-detect"
class MiddleService {
  async analyzeText(document, apiToken) {

    return axios.post(API_URL + "nlp",document,  {
      headers: {
        "x-api-key": apiToken,
        "Access-Control-Allow-Origin": "*",
        crossdomain: true,
      },
    });
  }
  async proccessing(text) {
    return axios.post(API_NLP, text);
  }
}
export default new MiddleService();
