import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VeeValidate from 'vee-validate';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap'
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import { BToast } from 'bootstrap-vue'
import PrettyCheck from 'pretty-checkbox-vue/check';
import Banner from './components/Banner';
import {
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faHome,
  faUserSecret,
  faKey,

} from '@fortawesome/free-solid-svg-icons'

import PrettyCheckbox from 'pretty-checkbox-vue';
import i18n from './i18n'



library.add(faUser, 
  faUserPlus,
  faSignInAlt,
  faSignOutAlt,
  faUserSecret,
  faKey,
  faHome)
// Optionally install the BootstrapVue icon components plugin

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(VeeValidate)
Vue.use(PrettyCheckbox);
Vue.config.productionTip = false
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('b-toast',BToast)
Vue.component('p-check',PrettyCheck);
Vue.component('m-banner',Banner);
new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
