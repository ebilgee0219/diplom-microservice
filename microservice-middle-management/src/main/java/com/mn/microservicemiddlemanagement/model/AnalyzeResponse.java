package com.mn.microservicemiddlemanagement.model;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnalyzeResponse {
    private String word;
    private String lemma;
    private String posTag;
    private String nameType;
    private String stopWordType;
}
