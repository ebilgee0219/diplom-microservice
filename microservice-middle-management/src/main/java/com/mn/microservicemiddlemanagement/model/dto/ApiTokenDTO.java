package com.mn.microservicemiddlemanagement.model.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ApiTokenDTO {

    private Long id;
    private Long userId;
    private String token;

    private Boolean status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;


    public boolean isBetween(Date now) {
        return ((this.startDate != null && now.after(startDate)) && (this.endDate != null && now.before(endDate)));
    }


}
