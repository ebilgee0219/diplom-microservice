package com.mn.microservicemiddlemanagement.model.dto;

import com.mn.microservicemiddlemanagement.model.AnalyzeService;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserRequestDTO {
    private Long userId;
    private List<String> usedServiceNames;
    private Date usedDate;
    private String context;
}
