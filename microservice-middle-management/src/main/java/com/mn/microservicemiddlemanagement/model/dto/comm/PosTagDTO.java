package com.mn.microservicemiddlemanagement.model.dto.comm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PosTagDTO {
    private String posTag;
}
