package com.mn.microservicemiddlemanagement.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class Document {

   private List<AnalyzeService> analyzeServices;
   private String content;
}
