package com.mn.microservicemiddlemanagement.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class NotAuthencated extends RuntimeException{
    public NotAuthencated() { super();}
    public NotAuthencated(String message,Throwable cause) { super(message,cause);}
    public NotAuthencated(String message) { super(message); }
    public NotAuthencated(Throwable cause) { super(cause); }
}
