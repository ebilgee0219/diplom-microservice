package com.mn.microservicemiddlemanagement.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ValidationError extends RuntimeException {
    public ValidationError() {
        super();
    }
    public ValidationError(String message, Throwable cause) {
        super(message,cause);
    }
    public ValidationError(String message) {
        super(message);
    }
    public ValidationError(Throwable cause) {
        super(cause);
    }
}
