package com.mn.microservicemiddlemanagement.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

public class APIKeyAuthFilter   extends AbstractPreAuthenticatedProcessingFilter {
    private final String principalRequestHeader;
    private final List<String> uriContains;
    private Logger logger= LoggerFactory.getLogger(APISecurityConfig.class);
    public APIKeyAuthFilter(String principalRequestHeader, List<String> uriContains) {
        this.principalRequestHeader = principalRequestHeader;
        this.uriContains = uriContains;
    }

    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return request.getHeader(principalRequestHeader);
    }

    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return "N/A";
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req= (HttpServletRequest) request;
        String uri=req.getRequestURI().toLowerCase();
        logger.info("uri-->" +uri );
        if(uriContains.stream().anyMatch(uri::contains)) {
            if(Objects.isNull(req.getHeader(principalRequestHeader)))
                logger.info("Required api token");
            super.doFilter(request, response, chain);
        }else chain.doFilter(request,response);
    }
}
