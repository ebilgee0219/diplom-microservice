package com.mn.microservicemiddlemanagement.config;

import com.mn.microservicemiddlemanagement.model.dto.ApiTokenDTO;
import com.mn.microservicemiddlemanagement.service.MiddleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

@Configuration
@EnableWebSecurity
public class APISecurityConfig extends WebSecurityConfigurerAdapter {
    @Value("${iapp.http.auth-token-header-name}")
    private String principalRequestHeader;

    private Logger logger = LoggerFactory.getLogger(APISecurityConfig.class);

    private final MiddleService middleService;

    public APISecurityConfig(MiddleService middleService) {
        this.middleService = middleService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        APIKeyAuthFilter filter = new APIKeyAuthFilter(principalRequestHeader, Collections.singletonList("/service/"));
        filter.setAuthenticationManager(authentication -> {
            //Хэрэглэгчийн оруулсан  api-key
            ApiTokenDTO apiTokenDTO = null;
            String principal = (String) authentication.getPrincipal();
            logger.debug(principal);
            if (Objects.isNull(principal))
                throw new BadCredentialsException("Api key is null");
            apiTokenDTO = this.middleService.getApitoken(principal);
            if (Objects.isNull(apiTokenDTO))
                throw new BadCredentialsException("Api token is not valid");

            if (apiTokenDTO.getStatus() == null || !apiTokenDTO.getStatus())
                throw new BadCredentialsException("Api key disabled");

            Date now = Calendar.getInstance().getTime();
            if (!apiTokenDTO.isBetween(now))
                throw new BadCredentialsException("Api key expired");

            if (!apiTokenDTO.getToken().equals(principal)) {
                throw new BadCredentialsException("The API key was not found or not the expected value.");
            }
            authentication.setAuthenticated(true);
            return authentication;
        });

        http.addFilter(filter);
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http/*.antMatcher("/service/*")*/.cors().and().csrf().disable();

        /*
        http.authorizeRequests().antMatchers("/swagger-ui.html/**","/public/**")
                .anonymous()
                .anyRequest().authenticated();*/
    }

    @Bean
    public GrantedAuthoritiesMapper grantedAuthoritiesMapper() {
        SimpleAuthorityMapper mapper = new SimpleAuthorityMapper();
        mapper.setConvertToUpperCase(true);
        return mapper;
    }

}

