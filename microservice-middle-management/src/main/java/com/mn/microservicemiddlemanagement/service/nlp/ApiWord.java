package com.mn.microservicemiddlemanagement.service.nlp;


import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.WordAnalyze;
import com.mn.microservicemiddlemanagement.model.dto.comm.WordDTO;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@Getter
@Setter
public class ApiWord {
    private final WordAnalyze wordAnalyze;
    private String serviceName;
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<WordDTO> wordDTOS;

    public ApiWord(WordAnalyze wordAnalyze) {
        this.wordAnalyze = wordAnalyze;
    }

    public List<WordDTO> callWord(String text) {
        try {
            this.wordDTOS = this.wordAnalyze.analyzeText(text);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return this.wordDTOS;
    }

}
