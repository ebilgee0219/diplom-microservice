package com.mn.microservicemiddlemanagement.service;

import com.mn.microservicemiddlemanagement.exceptions.ValidationError;
import com.mn.microservicemiddlemanagement.intercomm.UserClient;
import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.Lemma;
import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.NameType;
import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.PosTag;
import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.WordAnalyze;
import com.mn.microservicemiddlemanagement.model.AnalyzeResponse;
import com.mn.microservicemiddlemanagement.model.AnalyzeService;
import com.mn.microservicemiddlemanagement.model.Document;
import com.mn.microservicemiddlemanagement.model.dto.ApiTokenDTO;
import com.mn.microservicemiddlemanagement.model.dto.UserRequestDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.LemmaDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.NameTypeDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.PosTagDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.WordDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
public class MiddleService {

    private final UserClient userClient;
    private final WordAnalyze wordAnalyze;
    private final Lemma lemma;
    private final NameType nameType;
    private final PosTag posTag;
    private Logger logger = LoggerFactory.getLogger(MiddleService.class);
    private Long userId;

    @Async
    public CompletableFuture<String> findUserId(String name) {
        String userId = userClient.getUserId(name);
        return CompletableFuture.completedFuture(userId);
    }

    public List<AnalyzeResponse> jobAnalyze(Document document) {
        List<AnalyzeResponse> resultList = new ArrayList<>();
        if (Objects.isNull(document.getContent())) {
            throw new ValidationError("Бичвэр хоосон байна");
        }
        if (CollectionUtils.isEmpty(document.getAnalyzeServices()))
            throw new ValidationError("Анализ хийх сервис хоосон байна.");

        try {
            doAnalyzeByServices(document.getAnalyzeServices(), resultList, document.getContent());
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    public ApiTokenDTO getApitoken(String token) {
        ApiTokenDTO apiToken = null;
        try {
            apiToken = this.userClient.getApiTokenByToken(token);
            this.userId = apiToken.getUserId();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return apiToken;
    }
    @Cacheable("analyze")
    public void doAnalyzeByServices(List<AnalyzeService> analyzeServices, List<AnalyzeResponse> responses, String context) throws ExecutionException, InterruptedException {
        List<WordDTO> wordDTOS = null;
        List<LemmaDTO> lemmaDTOS = null;
        List<PosTagDTO> posTagDTOS = null;
        List<NameTypeDTO> nameTypeDTOS = null;
        List<String> nameServices = new ArrayList<>();
        int numberOfWords = 0;

        for (AnalyzeService val : analyzeServices) {
            nameServices.add(val.getName());
            if (val.getName().equals("word")) {
                CompletableFuture<List<WordDTO>> futureWord = CompletableFuture.supplyAsync(() -> callWord(context));
                wordDTOS = futureWord.get();
                if (numberOfWords == 0) numberOfWords = wordDTOS.size();
            }
            if (val.getName().equals("lemma")) {
                CompletableFuture<List<LemmaDTO>> futureLemma = CompletableFuture.supplyAsync(() -> callLemma(context));
                lemmaDTOS = futureLemma.get();
                if (numberOfWords == 0) numberOfWords = lemmaDTOS.size();
            }

            if (val.getName().equals("posTag")) {
                CompletableFuture<List<PosTagDTO>> futurePosTag = CompletableFuture.supplyAsync(() -> callPosTag(context));
                posTagDTOS = futurePosTag.get();
                if (numberOfWords == 0) numberOfWords = posTagDTOS.size();
            }
            if (val.getName().equals("nameType")) {
                CompletableFuture<List<NameTypeDTO>> futureNameType = CompletableFuture.supplyAsync(() -> callNameType(context));
                nameTypeDTOS = futureNameType.get();
                if (numberOfWords == 0) numberOfWords = nameTypeDTOS.size();
            }
        }
        //Userrequest
        UserRequestDTO userRequestDTO = buildUserRequest(context, nameServices);

        for (int i = 0; i < numberOfWords; i++) {
            AnalyzeResponse analyzeResponse = AnalyzeResponse.builder()
                    .build();
            if (!CollectionUtils.isEmpty(lemmaDTOS)) {
                analyzeResponse.setLemma(lemmaDTOS.get(i).getLemma());
            }
            if (!CollectionUtils.isEmpty(wordDTOS)) {
                analyzeResponse.setWord(wordDTOS.get(i).getWord());
                analyzeResponse.setStopWordType(wordDTOS.get(i).getStopWordType());
            }
            if (!CollectionUtils.isEmpty(posTagDTOS)) {
                analyzeResponse.setPosTag(posTagDTOS.get(i).getPosTag());
            }
            if (!CollectionUtils.isEmpty(nameTypeDTOS)) {
                analyzeResponse.setNameType(nameTypeDTOS.get(i).getNameType());
            }
            responses.add(analyzeResponse);
        }
        CompletableFuture.supplyAsync(() -> sendUserRequest(userRequestDTO)).thenApply(result -> {
            logger.info("Succesffuly added" + result.toString());
            return result;
        });
    }

    @Cacheable("callWord")
    public List<WordDTO> callWord(String text) {
        List<WordDTO> analyzeResponse = null;
        try {
            analyzeResponse = this.wordAnalyze.analyzeText(text);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return analyzeResponse;
    }

    @Cacheable("callLemma")
    public List<LemmaDTO> callLemma(String text) {
        List<LemmaDTO> response = null;
        try {
            response = this.lemma.analyzeText(text);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return response;
    }

    public List<NameTypeDTO> callNameType(String text) {
        List<NameTypeDTO> response = null;
        try {
            response = this.nameType.analyzeText(text);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return response;
    }

    @Cacheable("callPosTag")
    public List<PosTagDTO> callPosTag(String text) {
        List<PosTagDTO> response = null;
        try {
            response = this.posTag.analyzeText(text);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return response;
    }

    public UserRequestDTO sendUserRequest(UserRequestDTO dto) {
        try {
            dto = this.userClient.createUserRequest(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    public UserRequestDTO buildUserRequest(String context, List<String> serviceNames) {
        UserRequestDTO userRequestDTO = new UserRequestDTO();
        Date now = new Date();
        userRequestDTO.setUsedDate(formatDate(now));
        userRequestDTO.setContext(context);
        userRequestDTO.setUsedServiceNames(serviceNames);
        userRequestDTO.setUserId(this.userId);
        return userRequestDTO;
    }

    public Date formatDate(Date date) {
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat dtf = new SimpleDateFormat(pattern);
        String formated = dtf.format(date);
        try {
            date = dtf.parse(formated);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
