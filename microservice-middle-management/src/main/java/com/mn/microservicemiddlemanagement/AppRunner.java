package com.mn.microservicemiddlemanagement;

import com.mn.microservicemiddlemanagement.model.dto.ApiTokenDTO;
import com.mn.microservicemiddlemanagement.service.MiddleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class AppRunner implements CommandLineRunner {
    private final MiddleService middleService;
    private Logger logger = LoggerFactory.getLogger(AppRunner.class);

    public AppRunner(MiddleService middleService) {
        this.middleService = middleService;
    }
    @Override
    public void run(String... args) throws Exception {
        long start = System.currentTimeMillis();
//        try {
//            ApiTokenDTO token = this.middleService.getApitoken("jILAZeQJye");
//            logger.info("::::token: " + token.toString());
//        }catch (Exception e) {
//            logger.error(e.getMessage());
//        }
//        CompletableFuture<String> id1 = CompletableFuture.supplyAsync(() -> {
//            try {
//                return threadTest("form");
//            } catch (InterruptedException | ExecutionException e) {
//                throw new RuntimeException("E");
//            }
//        });
//        CompletableFuture<String> id2 = CompletableFuture.supplyAsync(() -> {
//            try {
//                return thread2("test");
//            } catch (InterruptedException | ExecutionException e) {
//                throw new RuntimeException("E");
//            }
//        });
//
//        CompletableFuture<String> id3 = middleService.findUserId("reg");
//
//        CompletableFuture<String> id4 = middleService.findUserId("Bill");
//
//        CompletableFuture<List<String>> list = middleService.findAllUsername();

//        CompletableFuture.allOf(id1, id2, id3, id4, list).join();
//
//        logger.info("Elapsed time: " + (System.currentTimeMillis() - start));
//        logger.info("-->:" + id1.get());
//        logger.info("-->:" + id2.get());
//        logger.info("-->:" + id3.get());
//        logger.info("-->:" + id4.get());
//        logger.info("-->:" + list.get());
    }

    public String threadTest(String name) throws InterruptedException, ExecutionException {
       // Thread.sleep(1000);
        CompletableFuture<String> id1 = middleService.findUserId(name);
        logger.info("-->!!!First Thread");
        return id1.get();
    }
    public String thread2(String name ) throws ExecutionException, InterruptedException {
        CompletableFuture<String> id1 = middleService.findUserId(name);
        logger.info("-->!!!Second Thead");
        return id1.get();
    }
}
