package com.mn.microservicemiddlemanagement.controller;

import com.mn.microservicemiddlemanagement.intercomm.UserClient;
import com.mn.microservicemiddlemanagement.intercomm.nlpcomm.NameType;
import com.mn.microservicemiddlemanagement.model.AnalyzeResponse;
import com.mn.microservicemiddlemanagement.model.Document;
import com.mn.microservicemiddlemanagement.model.dto.comm.LemmaDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.NameTypeDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.PosTagDTO;
import com.mn.microservicemiddlemanagement.model.dto.comm.WordDTO;
import com.mn.microservicemiddlemanagement.service.MiddleService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.ExecutionException;

@RequiredArgsConstructor
@RestController
@RequestMapping("service")
public class MiddleWareController {

    private final UserClient userClient;

    private final DiscoveryClient discoveryClient;

    private final Environment env;

    private final MiddleService middleService;

    @Value("${spring.application.name}")
    private String serviceId;

    private Logger logger = LoggerFactory.getLogger(MiddleWareController.class);

    @GetMapping("port")
    public String getPort() {
        return "Service is working at port :" + env.getProperty("local.server.port");
    }

    @GetMapping("instances")
    public ResponseEntity<?> getInstances() {
        return ResponseEntity.ok(discoveryClient.getInstances(serviceId));
    }

    @PostMapping("nlp")
    public ResponseEntity<List<AnalyzeResponse>> getAnalyzeText(@RequestBody Document document) {
        logger.info("---->>>>>Your Request:" + document.toString());
        List<AnalyzeResponse> result =   this.middleService.jobAnalyze(document);
        return ResponseEntity.ok(result);
    }

}
