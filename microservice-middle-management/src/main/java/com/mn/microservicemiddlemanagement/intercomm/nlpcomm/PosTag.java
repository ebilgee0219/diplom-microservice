package com.mn.microservicemiddlemanagement.intercomm.nlpcomm;

import com.mn.microservicemiddlemanagement.model.dto.comm.PosTagDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@FeignClient( "posTag-service")
public interface PosTag {
    @RequestMapping( method =  RequestMethod.POST, value = "/service/pos-tag-detect", consumes = "application/json")
    public List<PosTagDTO> analyzeText(@RequestBody String context) ;

}
