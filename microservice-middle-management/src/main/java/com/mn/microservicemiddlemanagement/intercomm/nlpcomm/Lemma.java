package com.mn.microservicemiddlemanagement.intercomm.nlpcomm;

import com.mn.microservicemiddlemanagement.model.dto.comm.LemmaDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(contextId ="lemmaClient" ,name="lemma-detect-service")
public interface Lemma {
    @RequestMapping( method =  RequestMethod.POST, value = "/service/lemma-detect", consumes = "application/json")
    public List<LemmaDTO> analyzeText(@RequestBody String context) ;

}
