package com.mn.microservicemiddlemanagement.intercomm.nlpcomm;

import com.mn.microservicemiddlemanagement.model.dto.comm.NameTypeDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("named-entity-service")
public interface NameType {
    @RequestMapping(method = RequestMethod.POST, value = "/service/name-type-detect", consumes = "application/json")
    public List<NameTypeDTO> analyzeText(@RequestBody String context);

}
