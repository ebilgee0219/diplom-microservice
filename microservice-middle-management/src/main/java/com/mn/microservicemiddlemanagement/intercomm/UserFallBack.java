package com.mn.microservicemiddlemanagement.intercomm;

import com.mn.microservicemiddlemanagement.exceptions.ResourceNotFound;
import com.mn.microservicemiddlemanagement.model.dto.ApiTokenDTO;
import com.mn.microservicemiddlemanagement.model.dto.UserRequestDTO;
import feign.FeignException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserFallBack implements UserClient {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Throwable cause;

    public UserFallBack(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public List<String> getUserNames(List<Long> useIdList) {
        return null;
    }

    @Override
    public String getUserId(String username) {
        return null;
    }

    @Override
    public List<String> getAllUsername() {
        return null;
    }

    @Override
    public ApiTokenDTO getApiTokenByToken(String token) {
        if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
            logger.error("404 error");
            throw new ResourceNotFound("404 ");
        }
        return new ApiTokenDTO();
    }

    @Override
    public UserRequestDTO createUserRequest(UserRequestDTO dto) {
        if(cause instanceof FeignException ) {
            logger.info("ERROR WHEN SEND USER USEREQUEST");
        }
        return null;
    }
}
