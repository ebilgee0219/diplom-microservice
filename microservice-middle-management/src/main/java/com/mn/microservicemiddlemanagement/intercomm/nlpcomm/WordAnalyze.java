package com.mn.microservicemiddlemanagement.intercomm.nlpcomm;

import com.mn.microservicemiddlemanagement.model.dto.comm.WordDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(contextId = "langDetectClient",name = "lang-detect-service")
public interface WordAnalyze {
    @RequestMapping( method =  RequestMethod.POST, value = "/service/lang-detect", consumes = "application/json")
    public List<WordDTO> analyzeText(@RequestBody String context) ;

}
