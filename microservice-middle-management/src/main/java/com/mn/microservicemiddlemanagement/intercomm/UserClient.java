package com.mn.microservicemiddlemanagement.intercomm;

import com.mn.microservicemiddlemanagement.model.dto.ApiTokenDTO;
import com.mn.microservicemiddlemanagement.model.dto.UserRequestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "user-service", fallbackFactory = UserFallBackFactory.class)
public interface UserClient {
    @RequestMapping(method = RequestMethod.POST, value = "/service/names", consumes = "application/json")
    public List<String> getUserNames(@RequestBody List<Long> useIdList);

    @RequestMapping(method = RequestMethod.POST, value = "/service/id", consumes = "application/json")
    public String getUserId(@RequestBody String username);

    @RequestMapping(method = RequestMethod.GET, value = "/service/usernames", consumes = "application/json")
    public List<String> getAllUsername();


    @RequestMapping(method = RequestMethod.POST, value = "/service/api-token/", consumes = "application/json")
    public ApiTokenDTO getApiTokenByToken(@RequestBody String token);

    @RequestMapping(method = RequestMethod.POST, value = "service/user-request", consumes = "application/json")
    public UserRequestDTO createUserRequest(@RequestBody UserRequestDTO dto);
}

