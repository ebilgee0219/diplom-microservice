package com.mn.lemma.controller;

import com.mn.lemma.model.ResponseDTO;
import com.mn.lemma.service.ApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("service")
public class ApiController {

    private final ApiService apiService;
    @PostMapping(value = "lemma-detect")
    public ResponseEntity<List<ResponseDTO>> analyzeText(@RequestBody String context) {
        List<ResponseDTO> result =  this.apiService.callNLP(context);
        return ResponseEntity.ok(result);
    }
}

